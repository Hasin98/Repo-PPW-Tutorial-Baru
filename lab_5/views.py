from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Todo
from lab_1.views import mhs_name
from django.views.generic.base import RedirectView

# Create your views here.
response = {}
def index(request):    
	response['author'] = mhs_name 
	todo = Todo.objects.all()
	response['todo'] = todo
	html = 'lab_5/lab_5.html'
	response['todo_form'] = Todo_Form
	return render(request, html, response)

def add_todo(request):
	form = Todo_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['title'] = request.POST['title']
		response['description'] = request.POST['description']
		todo = Todo(title=response['title'],description=response['description'])
		todo.save()
		return HttpResponseRedirect('/lab-5/')
	else:
		return HttpResponseRedirect('/lab-5/')

def delete_todo(request, id):
	todo_delete = Todo.objects.get(id=id)
	todo_delete.delete()

	return HttpResponseRedirect('/lab-5/')



