from django.shortcuts import render
from lab_1.views import mhs_name

# Create your views here.
response = {}
def index(request):    
	response['author'] = mhs_name 
	html = 'lab_6/lab_6.html'
	return render(request, html, response)