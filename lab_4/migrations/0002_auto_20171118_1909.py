# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-18 12:09
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 11, 18, 19, 9, 16, 135987, tzinfo=utc)),
        ),
    ]
