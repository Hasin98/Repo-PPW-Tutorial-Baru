from django.shortcuts import render
from datetime import datetime

# Enter your name here
mhs_name = 'Anas' 
birth_date = datetime.strptime("1998-06-03 00:00:00.000", "%Y-%m-%d %H:%M:%S.%f")
# TODO Implement this

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'myAge' : calculate_age(birth_date)}
    return render(request, 'index_lab1.html', response)


# TODO Implement this to complete last checklist
def calculate_age(birth_year):
	if (type(birth_year) != int):
		birth_year = birth_year.year
	myAge = datetime.now().year - birth_year
	return int(myAge)
    

